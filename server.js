const express = require('express');
const morgan = require('morgan');
const usersCrudRoute = require('./Routes/users_crud');


const app = express();
require('dotenv').config();
app.use(morgan('dev'));

const createError = require('http-errors');
const bodyParser = require('body-parser');
require('./DBConnections/mongoConnection');

app.use(bodyParser.json());
const port = process.env.PORT || 3000;

app.use('/auth', usersCrudRoute);

app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.send({
        error: {
            status: err.status || 500,
            message: err.message,
        },
    })
})

app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})
module.exports = app;
