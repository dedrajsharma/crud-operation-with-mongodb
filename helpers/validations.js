const Joi = require('@hapi/joi');

const validateUserBody = Joi.object({
    name: Joi.string().required().trim(),
    age: Joi.number().required()
})

const validateUserSearchBody = Joi.object({
    name: Joi.string()
})

const validateOrder = Joi.object({
    order: Joi.string().required().trim()
})

const validateForPagination = Joi.object({
    pageNumber: Joi.number().required(),
    pageSize: Joi.number().required()

})

module.exports = {
    validateUserBody, 
    validateUserSearchBody, 
    validateOrder, 
    validateForPagination
}