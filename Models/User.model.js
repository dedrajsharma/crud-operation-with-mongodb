const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

const UserSchema = new Schema({
    name: {
        type: String, 
        required: true, 
        lowercase: true
    },
    age: {
        type: Number, 
        required: true
    },
    created_date: { 
        type: Date, 
        default: Date.now
    },
    modified_date: { 
        type: Date, 
        default: Date.now
    },
    active: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    }
})

const User = mongoose.model('user', UserSchema);
module.exports = User;