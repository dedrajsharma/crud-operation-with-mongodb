const express = require('express');
const router = express.Router();
const createError = require('http-errors');
const User = require('../Models/User.model');
const { validateUserBody, validateUserSearchBody, validateOrder, validateForPagination } = require('../helpers/validations');

router.put('/add-user', async (req, res, next) => {
    try {
        //validating the incoming request
        const validateRequest = await validateUserBody.validateAsync(req.body); 
        if(!validateRequest.name || !validateRequest.age) throw next(createError.BadRequest("Please pass proper email or password"));
        
        //check in DB
        const doesExist = await User.findOne({name: validateRequest.name});
        if(doesExist) throw next(createError.Conflict(`${validateRequest.email} already exists`));

        const user = new User(validateRequest);
        const savedUser = await user.save();
        res.send({
            "id": savedUser.id,
            "name": savedUser.name,
            "age": savedUser.email,   
            "message": "User successfully saved" 
        });
    } catch (error) {
        if(error?.isJoi === true) error.status = 422;
        next(error);
    }
})

router.get('/find-user-by-name', async (req, res, next) => {
    try {
        //validating the incoming request
        const validateRequest = await validateUserSearchBody.validateAsync(req.query); 
        if(!validateRequest.name) throw next(createError.BadRequest("Please pass proper name to search in DB"));
        
        //check in Db 
        const foundusers = await User.find().where('name').in(validateRequest.name).exec();
        
        if(foundusers.length > 0) {
            res.send({
                "data": foundusers,   
                "message": "User/s successfully found" 
            });
        } else {
            res.send({
                "data": [],
                "message": "No such user exists" 
            });
        }
    } catch (error) {
        if(error?.isJoi === true) error.status = 422;
        next(error);
    }
})

router.get('/sort-users-by-age', async (req, res, next) => {
    try {
        //validating the incoming request
        const validateRequest = await validateOrder.validateAsync(req.query); 
        if(validateRequest.order != "asc" && validateRequest.order != "desc") throw next(createError.BadRequest("Please pass the order either desc/asc"));
        
        //check in DB
        const foundusers = await User.find().sort({age:validateRequest.order});
        
        if(foundusers.length > 0) {
            res.send({
                "data": foundusers,   
                "message": "User/s successfully found" 
            });
        } else {
            res.send({
                "data": [],
                "message": "No such user exists" 
            });
        }
    } catch (error) {
        if(error?.isJoi === true) error.status = 422;
        next(error);
    }
})

router.get('/get-all-users', async (req, res, next) => {
    try {
        //validating the incoming request
        const validateRequest = await validateForPagination.validateAsync(req.query); 
        if(!validateRequest) throw next(createError.BadRequest("Please pass pagenumber and pagesize"));
        
        //check in DB
        const pageNumber = parseInt(validateRequest.pageNumber) || 0;
        const limit = parseInt(validateRequest.pageSize) || 3;
        const result = {};
        const totalUsers = await User.countDocuments().exec();
        let startIndex = pageNumber * limit;
        const endIndex = (pageNumber + 1) * limit;
        result.totalUsers = totalUsers;
        if (endIndex < (await User.countDocuments().exec())) {
            result.next = {
                pageNumber: pageNumber + 1,
                limit: limit,
            };
        }
        result.data = await User.find().sort("_id").skip(startIndex).limit(limit).exec();
        result.rowsPerPage = limit;
        if(result.data.length > 0) {
            return res.json({
                data: result, 
                message: "Users Fetched successfully"
            });
        } else {
            return res.json({
                data: [], 
                message: "No records found" 
            });
        }
    } catch (error) {
        if(error?.isJoi === true) error.status = 422;
        next(error);
    }
})

module.exports = router;